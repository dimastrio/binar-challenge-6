package id.dimas.challenge6

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import id.dimas.challenge6.databinding.FragmentLoginBinding
import id.dimas.challenge6.helper.DataStoreManager
import id.dimas.challenge6.helper.viewModelsFactory
import id.dimas.challenge6.repo.UserRepo
import id.dimas.challenge6.viewmodel.LoginViewModel

class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val userRepo: UserRepo by lazy { UserRepo(requireContext()) }
    private val pref: DataStoreManager by lazy { DataStoreManager(requireContext()) }
    private val viewModel: LoginViewModel by viewModelsFactory { LoginViewModel(pref, userRepo) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
        moveToRegist()
        loginFunc()
    }


    private fun loginFunc() {
        binding.apply {
            btnLogin.setOnClickListener {
                val email = etEmail.text.toString()
                val password = etPassword.text.toString()
                when {
                    email.isEmpty() -> {
                        etEmail.error = "Email Tidak Boleh Kosong"
                    }
                    password.isEmpty() -> {
                        etPassword.error = "Password Tidak Boleh Kosong"
                    }
                    else -> {
                        viewModel.checkUserFromDb(email, password)
                    }
                }
            }
        }
    }

    private fun moveToRegist() {
        binding.apply {
            val btnRegister = tvGoToRegister

            btnRegister.setOnClickListener {
                etEmail.text.clear()
                etPassword.text.clear()
                it.findNavController()
                    .navigate(LoginFragmentDirections.actionLoginFragmentToRegisterFragment())
            }

        }
    }

    private fun observeData() {
        viewModel.succesMessage.observe(viewLifecycleOwner) {
            messageToast(it)
        }

        viewModel.failedMessage.observe(viewLifecycleOwner) {
            messageToast(it)
        }

        viewModel.getLogin().observe(viewLifecycleOwner) {
            if (it) {
                findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToHomeFragment())
            }
        }
    }


    private fun messageToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }


}