package id.dimas.challenge6.viewmodel

import androidx.lifecycle.*
import id.dimas.challenge6.database.User
import id.dimas.challenge6.helper.DataStoreManager
import id.dimas.challenge6.repo.UserRepo
import kotlinx.coroutines.launch

class ProfileViewModel(private val userRepo: UserRepo, private val pref: DataStoreManager) :
    ViewModel() {

    private val _updateMessage = MutableLiveData<String>()
    val updateMessage: LiveData<String> get() = _updateMessage

    private val _messageLogout = MutableLiveData<String>()
    val messageLogout: LiveData<String> get() = _messageLogout

    private var _user = MutableLiveData<User>()
    val user: LiveData<User> get() = _user


    fun logout() {
        viewModelScope.launch {
            pref.cleanPref()
        }
    }

    fun updateToDb(user: User) {
        viewModelScope.launch {
            val result = userRepo.updateUser(user)
            if (result != 0) {
                userRepo.checkEmailUser(user.email)
                _updateMessage.value = "Update Berhasil"
            }
        }
    }

//    fun getUser() {
//        viewModelScope.launch {
//            val email = sharedPref.getEmail(KEY_EMAIL)
//            _user.value = userRepo.getUser(email)
//        }
//    }

    fun getEmail(): LiveData<String> {
        return pref.getEmail().asLiveData()
    }

//    fun getUserId(): LiveData<Int> {
//        return pref.getUserId().asLiveData()
//    }
//
//    fun getPassword(): LiveData<String> {
//        return pref.getPassword().asLiveData()
//    }


    fun getData(email: String) {
        viewModelScope.launch {
            _user.value = userRepo.checkEmailUser(email)
        }
    }
}