package id.dimas.challenge6.viewmodel

import android.os.Bundle
import androidx.lifecycle.*
import id.dimas.challenge6.helper.DataStoreManager
import id.dimas.challenge6.model.Resource
import id.dimas.challenge6.repo.MovieRepo
import kotlinx.coroutines.Dispatchers

class HomeViewModel(private val pref: DataStoreManager, private val movieRepo: MovieRepo) :
    ViewModel() {

//    private var _user = MutableLiveData<String>()
//    val user: LiveData<String> get() = _user

    private val _movieBundle = MutableLiveData<Bundle>()
    val movieBundle: LiveData<Bundle> get() = _movieBundle


    fun getUsername(): LiveData<String> {
        return pref.getUsername().asLiveData()
    }
//    fun username() {
//        viewModelScope.launch {
//            val username = pref.getUsername().asLiveData().toString()
//            _user.value = "Welcome, $username"
//        }
//    }

    fun action(movie_id: Int) {
        val bundle = Bundle()
        bundle.putInt("movie_id", movie_id)
        _movieBundle.value = bundle
    }

    fun getAllMovie(key: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(movieRepo.getMovie(key)))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occured!"))
        }
    }


//    fun action(movie_id: Int) {
//        val mBundle = Bundle()
//        mBundle.putInt(getMovieId(), movie_id)
//        _movieBundle.value = mBundle
//    }
//
//    fun getMovieId(): LiveData<Int> {
//        return pref.getMovieId().asLiveData()
//    }
}