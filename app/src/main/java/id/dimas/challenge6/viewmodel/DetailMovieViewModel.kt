package id.dimas.challenge6.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import id.dimas.challenge6.model.DetailMovieItem
import id.dimas.challenge6.model.Resource
import id.dimas.challenge6.repo.MovieRepo
import id.dimas.challenge6.service.TMDBApiService
import kotlinx.coroutines.Dispatchers

class DetailMovieViewModel(
    private val apiService: TMDBApiService,
    private val movieRepo: MovieRepo
) : ViewModel() {

    private val _movie = MutableLiveData<DetailMovieItem>()
    val movie: LiveData<DetailMovieItem> get() = _movie

    private val _dataError = MutableLiveData<String>()
    val dataError: LiveData<String> get() = _dataError

//    fun getDetailMovie(movie_id: Int) {
//        apiService.getDetailMovie(movie_id, "072af9268186d5a57f828c9f3dd51aac")
//            .enqueue(object : Callback<DetailMovieItem> {
//                override fun onResponse(
//                    call: Call<DetailMovieItem>,
//                    response: Response<DetailMovieItem>
//                ) {
//                    if (response.isSuccessful) {
//                        if (response.body() != null) {
//                            _movie.value = response.body()
//                        } else {
//                            _dataError.postValue("Data Kosong")
//                        }
//                    } else {
//                        _dataError.postValue("Pengambilan Data Gagal")
//                    }
//                    Log.w("onResponse", "Berhasil")
//                }
//
//                override fun onFailure(call: Call<DetailMovieItem>, t: Throwable) {
//                    Log.w("onFailure", "Gagal")
//                }
//
//            })
//    }

    fun getDetailMovie(movie_id: Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(
                Resource.success(
                    movieRepo.getDetailMovie(
                        movie_id,
                        "072af9268186d5a57f828c9f3dd51aac"
                    )
                )
            )
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occured!"))
        }
    }
}