package id.dimas.challenge6.viewmodel

import androidx.lifecycle.*
import id.dimas.challenge6.helper.DataStoreManager
import id.dimas.challenge6.repo.UserRepo
import kotlinx.coroutines.launch

class LoginViewModel(private val pref: DataStoreManager, private val userRepo: UserRepo) :
    ViewModel() {

    private var _failedMessage = MutableLiveData<String>()
    val failedMessage: LiveData<String> get() = _failedMessage

    private var _succesMessage = MutableLiveData<String>()
    val succesMessage: LiveData<String> get() = _succesMessage

    fun checkUserFromDb(email: String, password: String) {
        viewModelScope.launch {
            val result = userRepo.checkEmailUser(email)
            if (result != null) {
                if (result.email == email && result.password == password) {
                    _succesMessage.value = "Login Berhasil"
                    pref.setData(result.userId!!, result.username, result.email, result.password)
                } else {
                    _failedMessage.value = "Username atau password kamu salah"
                }
            } else {
                _failedMessage.value = "Akun belum terdaftar"
            }
        }
    }

//    private fun setData(
//        userId: Int,
//        username: String,
//        email: String,
//        password: String
//    ) {
//        viewModelScope.launch {
//            pref.setData(userId,username, email, password)
//        }
//    }

    fun getLogin(): LiveData<Boolean> {
        return pref.prefLogin().asLiveData()
    }


}