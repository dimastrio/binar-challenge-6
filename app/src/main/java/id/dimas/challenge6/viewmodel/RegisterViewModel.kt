package id.dimas.challenge6.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.dimas.challenge6.database.User
import id.dimas.challenge6.repo.UserRepo
import kotlinx.coroutines.launch

class RegisterViewModel(private val userRepo: UserRepo) : ViewModel() {

    private var _successMessage = MutableLiveData<String>()
    val successMessage: LiveData<String> get() = _successMessage

    private var _failedMessage = MutableLiveData<String>()
    val failedMessage: LiveData<String> get() = _failedMessage

    fun saveUserToDb(
        username: String,
        email: String,
        password: String,
        fullName: String,
        dateBirth: String,
        address: String
    ) {
        val user = User(null, username, email, password, fullName, dateBirth, address)
        viewModelScope.launch {
            val emails = userRepo.checkEmailUser(email)
            if (emails == null) {
                val result = userRepo.insertUser(user)
                if (result != 0L) {
                    _successMessage.value = "Register Berhasil"
                }
            } else {
                _failedMessage.value = "Register Gagal"
            }
        }
    }
}