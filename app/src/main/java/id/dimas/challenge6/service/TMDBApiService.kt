package id.dimas.challenge6.service

import id.dimas.challenge6.model.DetailMovieItem
import id.dimas.challenge6.model.MovieResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TMDBApiService {

    @GET("movie/popular")
    suspend fun getAllMovie(@Query("api_key") key: String): MovieResponse

    @GET("movie/{movie_id}")
    suspend fun getDetailMovie(
        @Path("movie_id") movieId: Int,
        @Query("api_key") key: String
    ): DetailMovieItem

}