package id.dimas.challenge6.repo

import id.dimas.challenge6.service.TMDBApiService

class MovieRepo(private val apiService: TMDBApiService) {

    suspend fun getMovie(key: String) = apiService.getAllMovie(key)

    suspend fun getDetailMovie(movie_id: Int, key: String) =
        apiService.getDetailMovie(movie_id, "072af9268186d5a57f828c9f3dd51aac")
}